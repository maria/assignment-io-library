%define SYS_EXIT 60
%define SYS_WRITE 1
%define stdout 1

section .text

; Принимает код возврата и завершает текущий процесс
exit:
	mov rax, SYS_EXIT
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
.cnt:
	cmp byte [rdi+rax],0
	je .end
	inc rax
	jmp .cnt
.end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
        push rdi
	call string_length
        pop rsi
	mov rdx, rax
	mov rax, SYS_WRITE
	mov rdi, stdout
	syscall
	ret


print_newline
	mov rdi, '\n'
; Принимает код символа и выводит его в stdout
print_char:
        push rdi
        mov rsi, rsp
        mov rdi, stdout
        mov rdx, 1
        mov rax, SYS_WRITE
        syscall
        pop rdi
        ret

; Выводит знаковое 8-байтовое число в десятичном формате

print_int:
        mov rax, rdi
        test rax, rax
        jge .print_uns_int
.print_sign_int:
	push rdi
        mov rdi, '-'
        call print_char
	pop rdi
        neg rdi
.print_uns_int:

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
        push rax
        push rdi
        push 0x0
        mov rax, rdi
        mov rdi, 10
.to_str:
        xor rdx, rdx
        div rdi
        add rdx, '0'
        push rdx
        test rax, rax
        je .print_i
        jmp .to_str
.print_i:
        pop rdi
        test rdi, rdi
        je .end
        call print_char
        jmp .print_i
.end:   pop rdi
        pop rax
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
        push rbx
        push rdx

        xor rcx, rcx
        xor rdx, rdx
        xor rbx, rbx
.loop:
        mov  bl, [rdi + rcx]
        mov  dl, [rsi + rcx]
        cmp bl, 0x0
        je .res
        cmp dl, 0x0
        je .res
        cmp bl, dl
        jne .res
        inc rcx
        jmp .loop
.res:
        xor rcx, rcx
        cmp bl, dl
        je .pos
        pop rdx
        pop rbx
        mov rax, 0
        ret
.pos:
        pop rdx
        pop rbx
        mov rax, 1
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push  0
	mov rsi, rsp
	xor rdi, rdi
	mov rdx, 1
	xor rax, rax
	syscall
	pop rax
	ret



; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	xor rcx, rcx

.beg:
	push rdi
	push rsi
	push rcx
	call read_char
	pop rcx
	pop rsi
	pop rdi

	cmp rax, ' '
	je .beg
	cmp rax, 0x9
	je .beg
	cmp rax, '\n'
	je .beg
	cmp rax, 0
	je .out
.sym:
	cmp rax, 0
	je .out
	cmp rax, ' '
	je .out
	cmp rax, 0x9
	je .out
	cmp rax, '\n'
	je .out

	cmp rcx, rsi
	jge .err
	mov byte[rdi + rcx], al
	inc rcx

	push rdi
	push rsi
	push rcx
	call read_char
	pop rcx
	pop rsi
	pop rdi
	jmp .sym

.out:
	mov byte[rdi+rcx], 0
	mov rdx, rcx
	mov rax, rdi
	jmp .exit

.err:
	mov rax, 0
.exit:
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
        xor r8, r8
        xor rcx, rcx
        mov r10, 10
        xor rax, rax

.first:
        mov r8b, [rdi + rcx]
        inc rcx
        cmp r8b, '0'
        jb .excep
        cmp r8b, '9'
        ja .excep
        cmp r8b, 0
        je .excep
        sub r8b, '0'
        mov al, r8b

.next_att:
        mov r8b, [rdi + rcx]
        cmp r8b, '0'
        jb .retout
        cmp r8b, '9'
        ja .retout
        cmp r8b, 0
        je .retout
        inc rcx
        mul r10
        sub r8b, '0'
        add rax, r8
        jmp .next_att
.excep:
        mov rdx, 0
        jmp .exit

.retout:
        mov rdx, rcx

.exit:
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
        xor r8, r8
        xor rcx, rcx
        mov r10, 10
        xor rax, rax

.firstt:
        mov r8b, [rdi + rcx]
        cmp r8b, '-'
        je .read_sign_int
        jmp parse_uint

.read_sign_int:
        inc rdi
        call parse_uint
        cmp rdx, 0
        je .exit
        inc rdx
        neg rax

.exit:
        ret
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
        xor rax, rax
	push rdi
	call string_length
	pop rdi
	inc rax
	cmp rax, rdx
	jg .excep
.read:
        xchg rsi, rdi
	mov rcx, rax
	cld
	rep movsb
	jmp .exit
.excep:
        xor rax, rax
        jmp .exit
.exit:
        ret
